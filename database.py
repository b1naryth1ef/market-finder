from mongoengine import *
from config import obj

connect('farmapp', **obj)

prod_list = {
'Baked Goods':'baked',
'Cheese':'cheese',
'Crafts':'crafts',
'Flowers':'flowers',
'Seafood':'seafood',
'Fruit':'fruit',
'Herbs':'herbs',
'Vegetables':'vegetables',
'Honey':'honey',
'Jams':'jams',
'Maple':'maple',
'Meat':'meat',
'Nuts':'nuts',
'Plants':'plants',
'Prepared':'prepared',
'Soap':'soap'
}

class Market(Document):
	fmid = IntField()
	name = StringField()
	website = StringField()
	street = StringField()
	city = StringField() 
	county = StringField()
	state = StringField()
	zipcode = StringField() #This is a string because of xxxxxx-xxxx
	pos = GeoPointField() #We can do sexy queries with this
	loc = StringField() #This is a description

	#Information (Unk)
	credit = BooleanField()
	wic = BooleanField()
	wiccash = BooleanField()
	sfmnp = BooleanField()
	snap = BooleanField()

	#Information (Kn)
	baked = BooleanField() #Baked Goods
	cheese = BooleanField()
	crafts = BooleanField()
	flowers = BooleanField()
	seafood = BooleanField()
	fruit = BooleanField()
	herbs = BooleanField()
	vegetables = BooleanField()
	honey = BooleanField()
	jams = BooleanField()
	maple = BooleanField() #da fuq?
	meat = BooleanField()
	nuts = BooleanField()
	plants = BooleanField()
	prepared = BooleanField()
	soap = BooleanField()