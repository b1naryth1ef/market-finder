from openpyxl import load_workbook
import sys
from database import Market

wb = load_workbook(sys.argv[1])
ws = wb.get_active_sheet()

_swap = {
	'Y':True,
	'N':False,
}
swap = lambda x: _swap[x]

[1005876, 
u'10:10 Farmers Market', 
u'http://www.1010farmersmarket.com', 
u'5960 Stewart Parkway',
u'Douglasville', 
u'Douglas', 
u'Georgia', 
30135, 
-84.7689, 
33.7196, 
u'Faith-based institution (e.g., church, mosque, synagogue, temple)', 
u'Y', u'N', u'N', u'N', u'N', u'Y', u'Y', u'N', u'Y', u'N', u'Y', u'Y', u'Y', u'Y', u'Y', u'N', u'Y', u'N', u'Y', u'Y', u'Y']

#Market.drop_collection()

inty = 0
for row in ws.rows[5:]:
	inty += 1
	row = [i.value for i in row]
	obj = Market()
	obj.fmid = row[0]
	obj.name = row[1]
	obj.website = row[2]
	obj.street = row[3]
	obj.city = row[4]

	obj.county = row[5]
	obj.state = row[6]
	obj.zipcode = str(row[7])
	if row[8] and row[9]:
		obj.pos =  [float(row[8]), float(row[9])] #We can do sexy queries with this
	obj.loc =  row[10] #This is a description

	#Information (Unk)
	obj.credit = swap(row[11])
	obj.wic = swap(row[12])
	obj.wiccash = swap(row[13])
	obj.sfmnp = swap(row[14])
	obj.snap = swap(row[15])

	#Information (Kn)
	obj.baked = swap(row[16]) #Baked Goods
	obj.cheese = swap(row[17])
	obj.crafts = swap(row[18])
	obj.flowers = swap(row[19])
	obj.seafood = swap(row[20])
	obj.fruit = swap(row[21])
	obj.herbs = swap(row[22])
	obj.vegetables = swap(row[23])
	obj.honey = swap(row[24])
	obj.jams = swap(row[25])
	obj.maple = swap(row[26]) #da fuq?
	obj.meat = swap(row[27])
	obj.nuts = swap(row[28])
	obj.plants = swap(row[29])
	obj.prepared = swap(row[30])
	obj.soap = swap(row[31])
	obj.save()

print "%s objects added to database..." % inty