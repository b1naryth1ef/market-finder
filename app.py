from flask import Flask, render_template, request
from raven.contrib.flask import Sentry
from database import Market, prod_list
import sys, os, time
import json

app = Flask(__name__)
app.config['SENTRY_DSN'] = 'http://8db4f0e666484c0181d0432984ae7841:756f6401b7eb4fad885544e39e3ccd30@sentry.hydr0.com/3'
sentry = Sentry(app)

hashy = {
	True:'Yes',
	False:'No'
}

def renderFromQuery(q):
	return ["<a href=\"/market/%s\">%s</a>" % (i.fmid, i.name) for i in q[:10]]

def genProd(q):
	c = []
	for name in prod_list:
		i = prod_list[name]
		attr = getattr(q, i)
		c.append([name, hashy[attr]])
	return c

@app.route('/')
def root():
	return render_template('index.html')

@app.route('/market/<mid>')
def market(mid=None):
	if mid and mid.isdigit():
		q = Market.objects(fmid=int(mid))
		if len(q):
			return render_template('index.html', market=q[0], products=genProd(q[0]), credit=hashy[q[0].credit])
	return "Error!", 404 
		
@app.route('/search_loc', methods=['POST', "GET"])
def loc():
	x = request.args.get('x', 'fail')
	y = request.args.get('y', 'fail')
	try:
		x = float(x)
		y = float(y)
	except:
		return json.dumps({'items':[]})
	q = renderFromQuery(Market.objects(pos__near=(x, y)))
	
	return json.dumps({'items':q})

@app.route('/search', methods=['POST', 'GET'])
def search():
	args = {}
	if request.args.get('city'):
		args['city__icontains'] = request.args.get('city')
	if request.args.get('state'):
		args['state__icontains'] = request.args.get('state')
	if request.args.get('zip'):
		args['zipcode__icontains'] = request.args.get('zip')
	if not len(args): q = []
	else: q = renderFromQuery(Market.objects(**args))
	return json.dumps({'items':q})

if __name__ == "__main__":
    app.run(debug=True)