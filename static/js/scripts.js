$(document).ready(function() {
	$('#location').hide();
	$('#results').hide();
});

function SetText(obj, text) {
	$(obj).fadeOut(function() {
		$(this).text(text)
	}).fadeIn();
};

function DisplayResults(res) {
	var ul = $('<ul>').appendTo("#results");
	$(res.items).each(function(index, item) {
		ul.append(
			$(document.createElement('li')).html(item)
		);
	});
	$('#results').show("slow")
};

//Ajaxy-submission for the search forum
$('#submity').click(function() {
	$.get("search", $('.form').serializeArray(), function(result) {
		result = jQuery.parseJSON(result);
		if (result.items.length != 0) {
			$('#contactForm').hide("slow");
			SetText("#forminfo", "I found the following results for your search...")
			DisplayResults(result)
		} else {
			SetText("#forminfo", "No results found for your search! Try searching again?")
		};
	});
});

//Swap between tabs A
$('#bsearch').click(function() {
	$('#location').hide("slow");
	$('#form').show("slow");
	$('#results').html("");
	SetText("#btitle", "Manual Search")
});

//Swap between tabs B
$('#blocation').click(function() {
	$('#form').hide("slow");
	$('#location').show("slow");
	$('#results').html("")
	SetText("#btitle", "Geolocation Search")
});

//Get our location
$('#getloc').click(function() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(GetLocation, GetLocationError, {timeout:10000});
	} else {
		alert('Your browser does not support geolocation!')
	}
});

function GetLocationError(error) {
	$('#location').html("There was an error finding your location!");
};

function GetLocation(location) {
	$.get("search_loc", { x: location.coords.longitude, y: location.coords.latitude }, function(data) {
		result = jQuery.parseJSON(data);
		if (result.items.length != 0) {
			SetText("#locinfo", "I found the following results for your location...")
			DisplayResults(result)
		} else {
			SetText("#locinfo", "No results found for your location!")
		};
	});
};
